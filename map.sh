#!/bin/bash
# This Source Code Form is subject to the terms of the
# Mozilla Public License, v. 2.0. If a copy of the MPL
# was not distributed with this file, You can obtain one
# at https://mozilla.org/MPL/2.0/.

# set SRCINFO variable to anything (e.g., '1' or 'y') or 'export SRCINFO=1' or use argument/option '-si' or '--srcinfo' to enable (AUR) .SRCINFO generation (before make package)
[[ -z $SRCINFO ]] && SRCINFO=
# set SRCINFO_ONLY variable to anything (e.g., '1' or 'y') or 'export SRCINFO_ONLY=1' or use argument/option '-sio' or '--srcinfo-only' to ONLY generate (AUR) .SRCINFO (disable/skip make package)
[[ -z $SRCINFO_ONLY ]] && SRCINFO_ONLY=

help() {
  # dynamic (pseudorandom) package example (3); n=3 (n=number/integer)
  # optional coreutils (preferred/recommended) and built-in (fallback)
  pkg_example=()
  pkg_example_sort=()
  cups=
  pkg_example_str=
  if hash ls 2>/dev/null; then
    # bash 4.4+
    if hash mapfile 2>/dev/null; then
      # relative path
      #mapfile -t pkg_example < <(ls */PKGBUILD 2>/dev/null)
      #mapfile -t pkg_example < <(ls -- */PKGBUILD 2>/dev/null)
      # full path - './'
      mapfile -t pkg_example < <(ls ./*/PKGBUILD 2>/dev/null)
    fi
    # bash 3.x+ (fallback)
    # relative path
    #[[ -z ${pkg_example[*]} ]] && while IFS='' read -r line; do pkg_example+=("$line"); done < <(ls */PKGBUILD 2>/dev/null)
    #[[ -z ${pkg_example[*]} ]] && while IFS='' read -r line; do pkg_example+=("$line"); done < <(ls -- */PKGBUILD 2>/dev/null)
    # full path - './'
    [[ -z ${pkg_example[*]} ]] && while IFS='' read -r line; do pkg_example+=("$line"); done < <(ls ./*/PKGBUILD 2>/dev/null)
    # sort (coreutils) - printf + sort/shuf
    if hash printf 2>/dev/null && hash sort 2>/dev/null; then
      # bash 4.4+
      if hash mapfile 2>/dev/null; then
        #mapfile -t pkg_example_sort < <(printf "%s\n" "${pkg_example[@]}" 2>/dev/null | sort -u 2>/dev/null)
        # shuffle (coreutils)
        mapfile -t pkg_example_sort < <(printf "%s\n" "${pkg_example[@]}" 2>/dev/null | sort -Ru 2>/dev/null) && cups=1
        #hash shuf 2>/dev/null && mapfile -t pkg_example_sort < <(printf "%s\n" "${pkg_example[@]}" 2>/dev/null | shuf 2>/dev/null) && cups=1
      fi
      # bash 3.x+ (fallback)
      #[[ -z ${pkg_example_sort[*]} ]] && while IFS='' read -r line; do pkg_example_sort+=("$line"); done < <(printf "%s\n" "${pkg_example[@]}" 2>/dev/null | sort -u 2>/dev/null)
      # shuffle (coreutils)
      [[ -z ${pkg_example_sort[*]} ]] && while IFS='' read -r line; do pkg_example_sort+=("$line"); done < <(printf "%s\n" "${pkg_example[@]}" 2>/dev/null | sort -Ru 2>/dev/null) && cups=1
      #[[ -z ${pkg_example_sort[*]} ]] && hash shuf 2>/dev/null && while IFS='' read -r line; do pkg_example_sort+=("$line"); done < <(printf "%s\n" "${pkg_example[@]}" 2>/dev/null | shuf 2>/dev/null) && cups=1
      # pkg_example_sort -> pkg_example
      [[ -n ${pkg_example_sort[*]} ]] && [[ ${pkg_example_sort[*]} != "${pkg_example[*]}" ]] && pkg_example=("${pkg_example_sort[@]}")
    fi
  fi
  # set dynamic (pseudorandom) package example string if package(s) (p) greater than or equal to n (3); p>=n (n=3)
  if (( ${#pkg_example[@]} >= 3 )); then
    if [[ -n $cups ]]; then
      # presorted + preshuffled (coreutils)
      pkg_example_str1=${pkg_example[0]}
      pkg_example_str1=${pkg_example_str1#./}
      pkg_example_str1=${pkg_example_str1%/*}
      pkg_example_str2=${pkg_example[1]}
      pkg_example_str2=${pkg_example_str2#./}
      pkg_example_str2=${pkg_example_str2%/*}
      pkg_example_str3=${pkg_example[2]}
      pkg_example_str3=${pkg_example_str3#./}
      pkg_example_str3=${pkg_example_str3%/*}
      pkg_example_str="$pkg_example_str1 $pkg_example_str2 $pkg_example_str3"
    else
      # sort + shuffle AIO (built-in)
      # number of times to retry before giving up - default to ~100 retries per session (200); s=n-1 (n=3); r=s*100 (s=2)
      [[ ! $retry =~ ^[0-9]+$ ]] && retry=200
      # preliminary (pseudorandom) argument -> string - compare strings rather than (pseudorandom) argument numbers (no duplicate strings)
      pkg_example_str1=$(( RANDOM % ${#pkg_example[@]} ))
      pkg_example_str1=${pkg_example[$pkg_example_str1]}
      pkg_example_str1=${pkg_example_str1#./}
      pkg_example_str1=${pkg_example_str1%/*}
      pkg_example_str2=$(( RANDOM % ${#pkg_example[@]} ))
      pkg_example_str2=${pkg_example[$pkg_example_str2]}
      pkg_example_str2=${pkg_example_str2#./}
      pkg_example_str2=${pkg_example_str2%/*}
      pkg_example_str3=$(( RANDOM % ${#pkg_example[@]} ))
      pkg_example_str3=${pkg_example[$pkg_example_str3]}
      pkg_example_str3=${pkg_example_str3#./}
      pkg_example_str3=${pkg_example_str3%/*}
      while [[ $pkg_example_str1 == "$pkg_example_str2" ]] && (( retry > 0 )); do
        pkg_example_str2=$(( RANDOM % ${#pkg_example[@]} ))
        pkg_example_str2=${pkg_example[$pkg_example_str2]}
        pkg_example_str2=${pkg_example_str2#./}
        pkg_example_str2=${pkg_example_str2%/*}
        (( retry-- ))
      done
      while [[ $pkg_example_str1 == "$pkg_example_str3" || $pkg_example_str2 == "$pkg_example_str3" ]] && (( retry > 0 )); do
        pkg_example_str3=$(( RANDOM % ${#pkg_example[@]} ))
        pkg_example_str3=${pkg_example[$pkg_example_str3]}
        pkg_example_str3=${pkg_example_str3#./}
        pkg_example_str3=${pkg_example_str3%/*}
        (( retry-- ))
      done
      pkg_example_str="$pkg_example_str1 $pkg_example_str2 $pkg_example_str3"
      # give up (out of retries) - duplicate strings most likely culprit
      (( retry <= 0 )) && pkg_example_str=
    fi
  fi
  echo "  usage: $0 [package_dirname ...] [option ...] [makepkg_option ...]"
  if [[ -z $pkg_example_str ]]; then
    # static package example (fallback)
    if (( RANDOM % 2 == 0 )); then
      echo "example: $0 steamos-efi linux holo-keyring --srcinfo -f"
    else
      echo "example: $0 jupiter-hw-support linux-neptune mesa --srcinfo -f"
    fi
  else
    # dynamic package example
    echo "example: $0 $pkg_example_str --srcinfo -f"
  fi
  echo
  echo "options:"
  echo "  -h, --help            show this help message and exit"
  echo "  -c, --clean           clean up work files after build"
  echo "  -C, --Clean           remove \$srcdir/ dir before building the package"
  echo "  -f, --force           overwrite existing package"
  echo "  -i, --install         install package after successful build"
  echo "  -s, --syncdeps        install missing dependencies with pacman"
  echo "  -si, --srcinfo        enable (AUR) .SRCINFO generation (before make package)"
  echo "  -sio, --srcinfo-only  ONLY generate (AUR) .SRCINFO (disable/skip make package)"
  echo "  -v, --version         show version information and exit"
  echo
  # single line (1)
  echo "make all (found) package(s) if package (directory name (dirname)) not specified"
  #echo "if package (directory name (dirname)) not specified; make all (found) package(s)"
  # multiline (2)
  #echo -e "if package (directory name (dirname)) not specified:\n  make all (found) package(s)"
  echo
  #makepkg -h 2>/dev/null
  echo "see makepkg help ('makepkg -h', 'makepkg --help') for makepkg options"
  echo
}

make_pkg() {
  if [[ -n $SRCINFO || -n $SRCINFO_ONLY ]]; then
    #echo "generating $PWD (AUR) .SRCINFO ..."
    echo "generating ${PWD##*/} (AUR) .SRCINFO ..."
    makepkg --printsrcinfo > .SRCINFO
    if [[ -n $SRCINFO_ONLY ]]; then
      return
    #else
      # stray echo (newline) intentional
      #echo
    fi
    # stray echo (newline) intentional
    echo
  fi
  #makepkg $mp_clean $mp_Clean $mp_force $mp_install $mp_syncdeps "${mp_args[@]}"
  # syncdeps always on
  makepkg $mp_clean $mp_Clean $mp_force $mp_install -s "${mp_args[@]}"
}

make_all_pkg() {
  # find and make all package(s)
  echo -e "finding and making all package(s) ...\n"
  echo -e "searching for package(s) in $PWD ...\n"
  if [[ -s PKGBUILD ]]; then
    echo -e "making ${PWD##*/} package ...\n"
    make_pkg
    echo
  fi
  # relative path
  #if ! ls */PKGBUILD &>/dev/null; then
  #if ! ls -- */PKGBUILD &>/dev/null; then
  # full path - './'
  #if ! ls ./*/PKGBUILD &>/dev/null; then
  # relative path
  #if hash ls 2>/dev/null && ! ls */PKGBUILD &>/dev/null; then
  #if hash ls 2>/dev/null && ! ls -- */PKGBUILD &>/dev/null; then
  # full path - './'
  if hash ls 2>/dev/null && ! ls ./*/PKGBUILD &>/dev/null; then
    if [[ ! -s PKGBUILD ]]; then
      echo "no package(s) found!"
      echo
      exit 3
    fi
    exit
  fi
  for dir in */; do
    (
    # quietly continue (exit) if directory not found/accessible
    #cd "$dir" || exit
    # support directory starting with '-'
    cd -- "$dir" || exit
    # quietly continue (exit) if PKGBUILD not found or empty/null
    [[ ! -s PKGBUILD ]] && exit
    # basename
    dir_name=${PWD##*/}
    #echo -e "making ${dir::-1} package ...\n"
    echo -e "making $dir_name package ...\n"
    # output error and continue (exit) if PKGBUILD not found or empty/null
    #[[ ! -e PKGBUILD ]] && echo -e "error: $dir_name PKGBUILD not found!\n" && exit
    #[[ ! -s PKGBUILD ]] && echo -e "error: $dir_name PKGBUILD empty/null!\n" && exit
    make_pkg
    echo
    )
  done
}

# title
ds=
ds_b64=RHJha2UgU3RlZmFuaQo=
ds_hex=4472616b652053746566616e690a
# coreutils (primary)
hash base64 2>/dev/null && ds=$(echo $ds_b64 | base64 -d 2>/dev/null)
# BSD/Darwin coreutils (primary alternative)
[[ -z $ds ]] && hash gbase64 2>/dev/null && ds=$(echo $ds_b64 | gbase64 -d 2>/dev/null)
# OpenSSL (secondary fallback)
[[ -z $ds ]] && hash openssl 2>/dev/null && ds=$(echo $ds_b64 | openssl enc -base64 -d 2>/dev/null)
# (g)vim (tertiary fallback)
[[ -z $ds ]] && hash xxd 2>/dev/null && ds=$(echo $ds_hex | xxd -p -r 2>/dev/null)
# util-linux (quarternary fallback) - mirror
[[ -z $ds ]] && hash rev 2>/dev/null && ds=$(echo Valve | rev 2>/dev/null)
# ultimate (quinary) fallback
[[ -z $ds ]] && ds=evlaV
[[ $ds == evlaV ]] && li="    " || li=
echo
echo -e "      \e[1mM\e[0make(pkg) \e[1mA\e[0mrch \e[1mP\e[0mackage v0.3"
echo "$li Copyright (C) 2022-2023 $ds"
echo

# optional dependency check/nag
#if ! hash ls 2>/dev/null || ! hash printf 2>/dev/null || ! hash sort 2>/dev/null || ! hash shuf 2>/dev/null; then
if ! hash ls 2>/dev/null || ! hash printf 2>/dev/null || ! hash sort 2>/dev/null; then
  echo "warning: coreutils not found! coreutils recommended, but optional"
  echo
fi

# help options
for help; do
  case ${help,,} in
  -h|--help)
    help
    exit
    ;;
  -v|--version)
    exit
    ;;
  esac
done

# dependency check
if ! hash makepkg 2>/dev/null; then
  echo "error: makepkg (pacman) not found! makepkg (pacman) required!"
  echo "install makepkg (pacman) to continue"
  echo
  exit 1
fi

# verbose config
if [[ -n $SRCINFO ]]; then
  echo "(AUR) .SRCINFO generation (before make package) pre-enabled"
  echo
fi
if [[ -n $SRCINFO_ONLY ]]; then
  echo "ONLY generate (AUR) .SRCINFO (disable/skip make package) pre-enabled"
  echo
fi

# package (directory) name arguments and makepkg options
mp_clean=
mp_Clean=
mp_force=
mp_install=
#mp_syncdeps=
# syncdeps always on
#mp_syncdeps=-s
mp_args=()
pkg_dir=()
for arg; do
  case $arg in
  # makepkg (expanded) forward options
  -c|--clean)
    mp_clean=-c
    ;;
  -C|--Clean|--cleanbuild)
    mp_Clean=-C
    ;;
  -f|--force)
    mp_force=-f
    ;;
  -i|--install)
    mp_install=-i
    ;;
  -s|--syncdeps)
    # comment / do nothing (if syncdeps always on)
    #mp_syncdeps=-s
    ;;
  # options
  -si|--srcinf|--srcinfo)
    # verbose
    echo "(AUR) .SRCINFO generation (before make package) enabled"
    echo
    SRCINFO=1
    ;;
  -sio|--srcinf-only|--srcinfo-only|--srcinfonly|--srcinfoonly)
    # verbose
    echo "ONLY generate (AUR) .SRCINFO (disable/skip make package) enabled"
    echo
    SRCINFO_ONLY=1
    ;;
  # moved below - support directory starting with '-'
  # makepkg options
  #-*)
    # verbose
    #echo "unrecognized argument/option ($arg) specified"
    #echo "assuming makepkg option - forwarding ($arg) to makepkg"
    #echo
    #mp_args+=("$arg")
    #;;
  *)
    # make specific package (directory name (dirname))
    if [[ -d $arg ]]; then
      pkg_dir+=("$arg")
    # makepkg options
    elif [[ ${arg::1} == - ]]; then
      # verbose
      echo "unrecognized argument/option ($arg) specified"
      echo "assuming makepkg option - forwarding ($arg) to makepkg"
      echo
      mp_args+=("$arg")
    else
      echo "error: invalid argument/option ($arg) specified!"
      echo "specify package (directory name (dirname)) and/or makepkg option(s)"
      echo "check your entry and try again"
      echo
      # detect multiple (2+) arguments
      #if [[ $arg != "$*" ]]; then
      if (( ${#@} > 1 )); then
        echo "you entered:"
        echo "  $0 $*"
        echo "you entered argument(s)/option(s):"
        echo "  $*"
        echo
      fi
      help
      exit 2
    fi
    ;;
  esac
done

# find and make all package(s) if directory not specified
if [[ -z ${pkg_dir[*]} ]]; then
  # verbose
  echo -e "no package (directory name (dirname)) specified\n"
  make_all_pkg
else
  # make specific package(s) (directory name (dirname))
  for dir in "${pkg_dir[@]}"; do
    (
    # quietly continue (exit) if directory not found/accessible
    #cd "$dir" || exit
    # support directory starting with '-'
    cd -- "$dir" || exit
    # quietly continue (exit) if PKGBUILD not found or empty/null
    #[[ ! -s PKGBUILD ]] && exit
    # basename
    dir_name=${PWD##*/}
    #echo -e "making ${dir::-1} package ...\n"
    echo -e "making $dir_name package ...\n"
    # output error and continue (exit) if PKGBUILD not found or empty/null
    [[ ! -e PKGBUILD ]] && echo -e "error: $dir_name PKGBUILD not found!\n" && exit
    [[ ! -s PKGBUILD ]] && echo -e "error: $dir_name PKGBUILD empty/null!\n" && exit
    make_pkg
    echo
    )
  done
fi
