#!/usr/bin/env python
# minimum  :  Python 2.3+/2.7+ (requires module: portio) - portio requires Python >= 2.3/2.6

import argparse
import os
import sys

parser = argparse.ArgumentParser(usage='[sudo] %(prog)s [-h] [-l] [--help] [--lock]')
parser.add_argument('-l', '--lock', dest='lock', action='store_true', help='🔒 lock BIOS (reverse unlock) 🔒')
args = parser.parse_args()

try:
    import portio
except ImportError:
    print("error: Python module portio (dependency) not found!\n")
    sys.exit(1)

if os.geteuid() != 0:
    print("error: root required to 🔓 unlock/lock 🔒 Steam Deck (jupiter) BIOS (AMD CBS/PBS).\n")
    print(" info: run (as root) on Steam Deck (jupiter) hardware to 🔓 unlock/lock 🔒 BIOS (AMD CBS/PBS).\n")
    print("usage: [sudo] %s [-h] [-l] [--help] [--lock]\n" % sys.argv[0])
    sys.exit(2)

# https://gist.github.com/SmokelessCPUv2/8c1e6559031e199d9a678c9fe2ebf7d4
portio.ioperm(0x72, 2, 1)
portio.outb(0xf7, 0x72)
if args.lock:
    portio.outb(0x00, 0x73)
    print("Steam Deck (jupiter) BIOS successfully 🔒 locked 🔒 (AMD CBS/PBS).\n")
else:
    portio.outb(0x77, 0x73)
    print("Steam Deck (jupiter) BIOS successfully 🔓 unlocked 🔓 (AMD CBS/PBS).\n")
